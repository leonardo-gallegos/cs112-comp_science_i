#include <iostream>
#include "DynamicArray.h"

using namespace std;

int main() {
    DynamicArray<int> daInts;

    daInts.push_back(10);
    daInts.push_back(20);
    daInts.push_back(30);
    daInts.push_back(40);
    daInts.push_back(50);
    daInts.push_back(60);
    daInts.push_back(70);

    cout << daInts << endl;


    return 0;
}