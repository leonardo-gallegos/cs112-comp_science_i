#ifndef __MYSTACK_H__
#define __MYSTACK_H__

#include <vector>

using namespace std;

template<typename T>
class MyStack;

template<typename T>
MyStack<T> operator + (const MyStack<T> &s, const MyStack<T> &r) {
    MyStack<T> result = s;

    for(unsigned int i{0}; i < r.items.size(); i++)
    {
        result.items.push_back(r.items[i]);
    }
    return result;
}

template<typename T>
ostream& operator << (ostream &output, MyStack<T> &s){
    output << "[ ";
    for(unsigned int i = 0; i < s.items.size(); i++){
        output << s.items[i] << " "; 
    }
    output << " ]";
    return output;
}

template <typename T>
class MyStack {
        friend MyStack<T> operator + <>(const MyStack<T> &s, const MyStack<T> &r);
        friend ostream& operator << <>(ostream &output, MyStack<T> &s);
        vector<int> items;

    public:

        bool empty() const {
            return items.empty();
        }

        void push(const int &item) {
            items.push_back(item);
        }
 
        T pop() {
            T last = items.back();
            items.pop_back();
            return last;
        }

        void print(){
            for (int i{0}; i < items.size(); ++i){
                if (i == 0){
                    cout << "{ " << items[i] << " ";
                }
                else if (i + 1 == items.size()){
                    cout << ", " << items[i] << " }";
                }
                else{
                    cout << ", " << items[i] << " ";
                }
            }
            cout << endl;
        }
        /*
        MyStack<T> operator + (const MyStack<T> &s){
            MyStack<T> result = (*this);

            for(unsigned int i{0}; i < s.items.size(); i++)
            {
                result.items.push_back(s.items[i]);
            }
            return result;
        }
        */
};

#endif
