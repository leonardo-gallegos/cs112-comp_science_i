#include <iostream>
#include "MyStack.h"

using namespace std;

int main() {

    MyStack<int> s;
    MyStack<int> r;

    for (int i{1}; i <= 5; i++) 
        s.push(i);
    
    for (int i{5}; i > 0; i--) 
        r.push(i);

    cout << s.empty() << endl;

    s.print();

    s.pop();

    r.print();

    MyStack<int> t = s + r;

    t.print();

    cout << t;

    return 0;
}